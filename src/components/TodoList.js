import React, { useEffect, useState } from "react";
import TodoInput from "./TodoInput";
import Todo from './Todo';
import './TodoList.css';
import {CSSTransition, TransitionGroup} from 'react-transition-group';
import {useSelector, useDispatch} from 'react-redux';
import {completeTodo, addTodo, removeTodo} from '../redux/action';
import {v4 as uuidv4} from 'uuid';

const TodoList = () => {
    const state = useSelector((state) => ({...state.todos}));
    const [todoList, setTodoList] = useState([]);
    const LIST_TASK = process.env.REACT_APP_API;

    useEffect(() => {
        fetch(LIST_TASK, {
            method: 'GET',
        }).then(res => res.json()).then(data => {
            setTodoList(data)
    }).catch((err) => { console.log('error', err)})
    }, [state.todos]);

    let dispatch = useDispatch();

    const create = async (data) => {
        const newTodo = {
            id: uuidv4(),
            task: data,
            completed: false
        };
        var options = {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          redirect: "follow",
          body: JSON.stringify(newTodo),
        };
        fetch(LIST_TASK, options)
          .then(function (response) {
            dispatch(addTodo(newTodo));
            response.json();
          });
    }

    const remove = async (data) => {     
        var options = {
            method: "DELETE",
            headers: {
              "Content-Type": "application/json",
            },
          };
        fetch(LIST_TASK+'/'+data.id, options)
          .then(function (response) {
            dispatch(removeTodo(data));
            response.json();
          });
    }

    const completeTask = (todo) => {
        const newTodo = {
            id: todo.id,
            task: todo.task,
            completed: !todo.completed
        };
        console.log(newTodo)
        var options = {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          redirect: "follow",
          body: JSON.stringify(newTodo),
        };
        fetch(LIST_TASK+'/'+todo.id, options)
          .then(function (response) {
            dispatch(completeTodo(todo))
            response.json();
          });
    }

    return (
        <div className="TodoList">
            <h1>TodoList App</h1>
            <TodoInput createTodo={create} />
            <ul>
               <TransitionGroup className="todo-list">
                    {todoList && todoList.length && todoList.map((todo) => {
                        return (
                            <CSSTransition key={todo.id} classNames="todo">
                                <Todo 
                                key={todo.id}
                                id={todo.id}
                                task={todo.task}
                                completed={todo.completed}
                                toggleTodo={() => completeTask(todo)}
                                removeTodo={() => remove(todo)}
                                />
                            </CSSTransition>
                        )
                    })}
               </TransitionGroup>
            </ul>
        </div>
    )
}

export default TodoList;