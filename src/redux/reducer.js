import { createSlice, PayloadAction } from '@reduxjs/toolkit'

const initiaState = {
    todos: []
};

export const todosReducer = createSlice({
    name: 'todo',
    initiaState,
    reducers: {
        addTodo: (state, action) => {
            const addedTodo = [...state.todos, action.payload];
            return {
                ...state,
                todos: addedTodo,
            };
    
        },
        completeTodo: (state, action) => {
            const toggleTodo = state.todos.map((t) => 
            t.id === action.payload.id ? {...action.payload, completed: !action.payload.completed} : t);
            return {
                ...state,
                todos: toggleTodo,
            };
        },
        removeTodo: (state, action) => {
            const filterTodo = state.todos.filter((t) => t.id !== action.payload.id);
            return {
                ...state,
                todos: filterTodo,
            }
        },
      },

})

export default todosReducer